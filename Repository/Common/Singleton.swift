//
//  Singleton.swift
//  Repository
//
//  Created by Satish Kumar on 3/17/18.
//  Copyright © 2018 Satish Kumar. All rights reserved.
//

import UIKit

class Singleton :NSObject{
    static let sharedInstance = Singleton()
    var noOfPage : NSInteger = 1
    var isServerCallInProgress : NSInteger = 0
    var currentRepoString : String = "apple"
    var priviousRepoString : String = "apple"

    func createFinalUrlString(pageNo: NSInteger) -> String {
        let endPointUrl: String = "https://api.github.com/users/" + Singleton.sharedInstance.currentRepoString + "/repos?page=" + String(pageNo) + "&per_page=10"
        return endPointUrl
    }
    
    // MARK: - Server Call
    func fetchDataFromTheServer(completionBlock: @escaping (_ succeeded: Bool, _ dataArray: [RepoDataModel]?) -> Void) {
        // Set up the URL request
        Singleton.sharedInstance.isServerCallInProgress = 1
        let finalUrl: String = Singleton.sharedInstance.createFinalUrlString(pageNo: Singleton.sharedInstance.noOfPage)
        
        guard let url = URL(string: finalUrl) else {
            print("Error: cannot create URL")
            return
        }
        
        var urlRequest = URLRequest.init(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 300)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            
            print(responseData)
            // parse the result as JSON, since that's what the API provides. Bind it with model class
            do {
                var repoDataModelArray : [RepoDataModel] = []
                if let dataArray = try JSONSerialization.jsonObject(with: responseData, options:.mutableContainers) as? [[String: Any]]{
                    for item in dataArray{
                        var name : String! = ""
                        var desc : String! = ""
                        var created_at : String! = ""
                        var licName : String! = ""
                        var licDic : [String: Any]?
                        
                        if let _ = item["name"] as? String{
                            name = item["name"] as? String
                        }
                        
                        if let _ = item["description"] as? String{
                            desc = item["description"] as? String
                        }
                        
                        if let _ = item["created_at"] as? String{
                            created_at = item["created_at"] as? String
                        }
                        
                        if let _ = item["license"] {
                            licDic = item["license"] as? [String: Any]
                        }
                        
                        if let _ = licDic {
                            licName = licDic!["name"] as? String
                        }
                        let repoDataModel = RepoDataModel(name: name, descript: desc, createdAt: created_at, licence: licName)
                        repoDataModelArray.append(repoDataModel)
                    }
                    completionBlock(true,repoDataModelArray)

                }else{
                    completionBlock(false,nil)
                    return
                }
            } catch  {
                completionBlock(false,nil)
                print("error trying to convert data to JSON")
                return
            }
        }
        task.resume()
    }
    

}
