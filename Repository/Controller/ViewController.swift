//
//  ViewController.swift
//  Repository
//
//  Created by Satish Kumar on 3/16/18.
//  Copyright © 2018 Satish Kumar. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var homePageCollectionView: UICollectionView!
    @IBOutlet weak var textInput: UITextField!
    
    var itemsPerRow: CGFloat = 1
    let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    let reuseIdentifier = "HomePageCollectionViewCell" // also enter this string as the cell identifier in the storyboard
    var dataArrayForCollectionView : [RepoDataModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        segmentedControl.selectedSegmentIndex=0
        tapGridOrListSegment(segmentedControl)
        textInput.delegate=self
        self.homePageCollectionView.contentInsetAdjustmentBehavior = .always
    
        //Server call
        Singleton.sharedInstance.fetchDataFromTheServer { (_ succeeded: Bool, _ dataArray: [RepoDataModel]?) in
            self.refreshCollectionView(succeeded, dataArray)
        }
    }
    
    func refreshCollectionView(_ succeeded: Bool, _ dataArray: [RepoDataModel]?){
        if succeeded{
            if Singleton.sharedInstance.priviousRepoString != Singleton.sharedInstance.currentRepoString{
                self.dataArrayForCollectionView = dataArray
            }else{
                if let _ = self.dataArrayForCollectionView{
                    self.dataArrayForCollectionView = self.dataArrayForCollectionView + dataArray!
                }else {
                    self.dataArrayForCollectionView = dataArray
                }
            }
            DispatchQueue.main.async {
                self.textInput.text=""
                
                self.homePageCollectionView.reloadData()
                Singleton.sharedInstance.isServerCallInProgress = 0
                Singleton.sharedInstance.priviousRepoString = Singleton.sharedInstance.currentRepoString
                
            }
        } else {
            DispatchQueue.main.async {
                self.textInput.text=""
                let alert = UIAlertController(title: "No Data Found.", message: "", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                
                self.present(alert, animated: true)
            }
            Singleton.sharedInstance.currentRepoString = Singleton.sharedInstance.priviousRepoString
        }
}
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let _ = self.dataArrayForCollectionView{
            return self.dataArrayForCollectionView.count
        }else{
            return 0
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! HomePageCollectionViewCell
        cell.descript.numberOfLines=0;
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        var name : String? = ""
        var desc : String? = ""
        var created_at : String? = ""
        var licName : String? = ""
        
        if let _ = self.dataArrayForCollectionView[indexPath.item].name{
            name = self.dataArrayForCollectionView[indexPath.item].name
        }
        
        if let _ = self.dataArrayForCollectionView[indexPath.item].descript{
            desc = self.dataArrayForCollectionView[indexPath.item].descript
        }
        
        if let _ = self.dataArrayForCollectionView[indexPath.item].createdAt{
            created_at = self.dataArrayForCollectionView[indexPath.item].createdAt
        }
        
        if let _ = self.dataArrayForCollectionView[indexPath.item].licence {
            licName = self.dataArrayForCollectionView[indexPath.item].licence
        }
        
        
        cell.name.text = "Name: " + name!
        cell.descript.text = "Desc.: " + desc!
        cell.licence.text = "Licence: " + licName!
        cell.createdAt.text = "Created At: " + created_at!
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.item)!")
        textInput.text=""
        self.view.endEditing(true)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height: CGFloat = 210.0
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = homePageCollectionView.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        var heightPerItem = widthPerItem
        
        var height1: CGFloat = 30.0
        var height2: CGFloat = 30.0
        var height3: CGFloat = 30.0
        var height4: CGFloat = 50.0
        
        if let text1 = self.dataArrayForCollectionView[indexPath.item].name {
            height1 = estimateFrameForText(text: text1).height + 10.0
        }
        
        if let text2 = self.dataArrayForCollectionView[indexPath.item].descript {
            height2 = estimateFrameForText(text: text2).height + 10.0
        }
        
        if let text3 = self.dataArrayForCollectionView[indexPath.item].createdAt {
            height3 = estimateFrameForText(text: text3).height + 30.0
        }
        
        if let text4 = self.dataArrayForCollectionView[indexPath.item].licence {
            height4 = estimateFrameForText(text: text4).height + 10.0
        }
        
        height = height1 + height2 + height3 + height4 + paddingSpace
        
        //*I am hardcoding multiplier bcz of the time shortage. Ideally, multiplier should be calculated based on the text sixe.  *//
       /* switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            if UIDevice.current.orientation.isLandscape {
                heightPerItem = heightPerItem * 0.25  //*I am hardcoding multiplier bcz of the time shortage. Ideally, multiplier should be calculated based on the text sixe.  *//
            } else {
                heightPerItem = heightPerItem * 0.5
            }
        case 1:
            if UIDevice.current.orientation.isLandscape {
                heightPerItem = heightPerItem * 0.65
            } else {
                heightPerItem = height
            }
        default:
            break
        }*/
        return CGSize(width: widthPerItem, height: height)
    }
    
    private func estimateFrameForText(text: String) -> CGRect {
        
        let height: CGFloat = 210.0
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = homePageCollectionView.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        let size = CGSize(width: widthPerItem, height: height)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let attributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)]
        
        return NSString(string: text).boundingRect(with: size, options: options, attributes: attributes, context: nil)
    }

    
    // MARK: - UICollectionViewDelegateFlowLayout protocol
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    //Re-calculate item size in case of device rotation
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        guard let flowLayout = homePageCollectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayout.invalidateLayout()
    }
    
    // MARK: - //Pagination feature
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height + 70.0)) {
            //reach bottom
            if Singleton.sharedInstance.isServerCallInProgress == 0{
                Singleton.sharedInstance.noOfPage += 1
                Singleton.sharedInstance.fetchDataFromTheServer { (_ succeeded: Bool, _ dataArray: [RepoDataModel]?) in
                    self.refreshCollectionView(succeeded, dataArray)
                }
            }
        }
        
    }
    
    // MARK: - UITextFieldDelegate protocol
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print(textInput.text!)
        if let _ = textInput.text?.count{
            Singleton.sharedInstance.currentRepoString = textInput.text!
            Singleton.sharedInstance.fetchDataFromTheServer { (_ succeeded: Bool, _ dataArray: [RepoDataModel]?) in
                self.refreshCollectionView(succeeded, dataArray)
            }
        }
        textInput.resignFirstResponder()
        return true
    }
    
    //This is for the keyboard to GO AWAYY !! when user clicks anywhere on the view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        textInput.text=""
        self.view.endEditing(true)
    }
    
    @IBAction func tapGridOrListSegment(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            itemsPerRow = 1
            segmentedControl.selectedSegmentIndex=0
            self.homePageCollectionView.reloadData()
            print("1")
        case 1:
            print("2")
            itemsPerRow = 2
            segmentedControl.selectedSegmentIndex=1
            self.homePageCollectionView.reloadData()
            
        default:
            break
        }
    }
    
 
}

