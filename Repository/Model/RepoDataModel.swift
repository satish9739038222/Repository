//
//  RepoDataModel.swift
//  Repository
//
//  Created by Satish Kumar on 3/21/18.
//  Copyright © 2018 Satish Kumar. All rights reserved.
//

import Foundation
class RepoDataModel {
    var name: String?
    var descript: String?
    var createdAt: String?
    var licence: String?
    
    init(name: String, descript: String, createdAt: String, licence: String) {
        self.name = name
        self.descript = descript
        self.createdAt = createdAt
        self.licence = licence
    }
}


