//
//  HomePageCollectionViewCell.swift
//  Repository
//
//  Created by Satish Kumar on 3/17/18.
//  Copyright © 2018 Satish Kumar. All rights reserved.
//

import UIKit

class HomePageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var descript: UILabel!
    @IBOutlet weak var createdAt: UILabel!
    @IBOutlet weak var licence: UILabel!
    
}
