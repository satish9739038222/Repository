//
//  RepositoryTests.swift
//  RepositoryTests
//
//  Created by Satish Kumar on 3/22/18.
//  Copyright © 2018 Satish Kumar. All rights reserved.
//

import XCTest
@testable import Repository

class RepositoryTests: XCTestCase {
     var systemUnderTest: ViewController!
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        //get the storyboard the ViewController under test is inside
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        //get the ViewController we want to test from the storyboard (note the identifier is the id explicitly set in the identity inspector)
        systemUnderTest = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        //load view hierarchy
        _ = systemUnderTest.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSUT_CanInstantiateViewController() {
        
        XCTAssertNotNil(systemUnderTest)
    }
    
    func testSUT_CollectionViewIsNotNilAfterViewDidLoad() {
        
        XCTAssertNotNil(systemUnderTest.homePageCollectionView)
    }
    
    func testSUT_HasItemsForCollectionView() {
        
        XCTAssertNotNil(systemUnderTest.dataArrayForCollectionView)
    }
    
    func testSUT_ShouldSetCollectionViewDataSource() {
        
        XCTAssertNotNil(systemUnderTest.homePageCollectionView.dataSource)
    }
    
    func testSUT_ConformsToCollectionViewDataSource() {
        
        XCTAssert(systemUnderTest.conforms(to: UICollectionViewDataSource.self))
    }
    
    func testSUT_ShouldSetCollectionViewDelegate() {
        
        XCTAssertNotNil(systemUnderTest.homePageCollectionView.delegate)
    }
    
    func testSUT_ConformsToCollectionViewDelegate() {
        XCTAssert(systemUnderTest.conforms(to: UICollectionViewDelegate.self))
    }
    
    func testSUT_ConformsToCollectionViewDelegateFlowLayout () {
        
        XCTAssert(systemUnderTest.conforms(to: UICollectionViewDelegateFlowLayout.self))

    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
